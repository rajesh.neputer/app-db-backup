#!/bin/bash
set -eu
# Docs: http://linuxcommand.org/lc3_man_pages/seth.html


help() {
  cat << EOF
Backup application directory
usage: $0 [OPTIONS]
    --help               Show this message
EOF
}


for opt in "$@"; do
  case $opt in
    --help|-h)
      help
      exit 0
      ;;
    *)
      echo "unknown option: $opt"
      help
      exit 1
      ;;
  esac
done


# FOLDER_DATE_FORMAT
[[ -n "$(./get_env.sh APP_BACKUP_FOLDER_DATE_FORMAT)" ]] && FOLDER_DATE_FORMAT="$($(./get_env.sh APP_BACKUP_FOLDER_DATE_FORMAT))" || FOLDER_DATE_FORMAT="$(date +%F)"

# FILE_DATE_FORMAT
[[ -n "$(./get_env.sh APP_BACKUP_FILE_DATE_FORMAT)" ]] && FILE_DATE_FORMAT="$($(./get_env.sh APP_BACKUP_FILE_DATE_FORMAT))" || FILE_DATE_FORMAT="$(date +%F_%H-%M-%S)"

# Source App Location
APP_LOCATION=("$(./get_env.sh APP_APP_LOCATION)")

getAppName() {
	IFS='/' read -ra ADDR <<< "${1}"	
	echo ${ADDR[-1]}
}

exclude_flags=""
excludeDirectory() {
	exclude=("$(./get_env.sh APP_APP_EXCLUDE_DIR)")

	for i in ${exclude[@]}; do
		exclude_flags="${exclude_flags} --exclude=$i"
	done

}

# todo: includeDirectory is not in use
include_directories=""
includeDirectory() {
	include=("$(./get_env.sh APP_APP_INCLUDE_DIR)")
	
	for i in ${include[@]}; do

		IFS=':' read -ra ADDR <<< "$i"
		for min_i in "${ADDR[@]}"; do
	
			include_directories="${include_directories} $min_i"

		done		

		# echo $include_directories;
		# exit 1;

	done

}


purgeOldBackups() {
	### Delete all backups older than "APP_MAX_BACKUP_DAYS" 
	# older_backups="find ${APP_BACKUP_PATH} -maxdepth 1 -type d -mtime +${APP_MAX_BACKUP_DAYS}"

	APP_BACKUP_PATH="$1/*"
	[[ -n "$(./get_env.sh APP_MAX_BACKUP_DAYS)" ]] && APP_MAX_BACKUP_DAYS="$(./get_env.sh APP_MAX_BACKUP_DAYS)" || APP_MAX_BACKUP_DAYS="15"

	find_exp="find ${APP_BACKUP_PATH} -maxdepth 1 -type d"
	total_backups="$($find_exp | wc -l)" 			# total backup count
	older_backups="$find_exp -mtime +${APP_MAX_BACKUP_DAYS}"		# older than X days
	older_backups_count="$($(echo "${older_backups}") | wc -l)"
	newer_backups_count="$($(echo "$find_exp -mtime -${APP_MAX_BACKUP_DAYS}") | wc -l)"

	# remaining(newer) backups must be equal or greater than max_backup days
	if [[ "${newer_backups_count}" -ge ${APP_MAX_BACKUP_DAYS} ]]; then
		echo "Deleting $older_backups older backups";
		$(echo "${older_backups}") | xargs rm -rf
	else
		echo "Older backups are not found and can not be deleted."
	fi
	
}

uploadToS3() {
	# Backup if enabled
	if [[ -n "$(./get_env.sh APP_S3_BACKUP)" && $(./get_env.sh APP_S3_BACKUP) == "true" ]]; then

		FILE_NAME="$(echo $1 | rev | cut -d "/" -f1-3 | rev | sed -e 's/\//_/g')"
		FOLDER_PATH="$(echo $1 | rev | cut -d "/" -f2- | cut -d "/" -f1-2 | rev)"
		FILE_PATH=$1

		if [[ -n "$(./get_env.sh USE_CUSTOM_S3CMD)" && $(./get_env.sh USE_CUSTOM_S3CMD) == "true" ]]; then
			S3_BIN_FILE="../bin/s3cmd-2.2.0/s3cmd"
		else 
			S3_BIN_FILE="s3cmd"
		fi
		
		$S3_BIN_FILE -c "$(./get_env.sh APP_S3_CONFIG_FILE)" put "${FILE_PATH}" "$(./get_env.sh APP_S3_LOCATION)"${FOLDER_PATH}/
		UPLOAD_STATUS=$?
		# if secondary config exists; proceed the backup
		if [[ -n "$(./get_env.sh APP_S3_SECONDARY_CONFIG_FILE)" ]]; then
			$S3_BIN_FILE -c "$(./get_env.sh APP_S3_SECONDARY_CONFIG_FILE)" put "${FILE_PATH}" "$(./get_env.sh APP_S3_SECONDARY_LOCATION)"${FOLDER_PATH}/
		fi

		MESSAGE=""
		if [[ ${UPLOAD_STATUS} -eq 0 ]]; then
			MESSAGE="${FILE_NAME} successully uploaded to S3";
		else
			MESSAGE="Failed to upload ${FILE_NAME} to S3";
		fi
		sendSlackNotification "${UPLOAD_STATUS}" "aws" "${MESSAGE}" ""

	fi
}

sendSlackNotification() {
	BACKUP_STATUS=$1		# Here backup status means backup or upload status
	TYPE=$2
	MESSAGE=$3
	DATE=$4

	# Send if enabled
	if [[ -n "$(./get_env.sh APP_SLACK_NOTIF)" && $(./get_env.sh APP_SLACK_NOTIF) == "true" ]]; then

		# Check whether to send a notification for success event
		if [[ "$BACKUP_STATUS" -eq 0 ]]; then

			if [[ -z "$(./get_env.sh APP_HIDE_SUCCESS_SLACK_NOTIF)" || "$(./get_env.sh APP_HIDE_SUCCESS_SLACK_NOTIF)" != "true" ]]; then
				./slackhook.sh "${BACKUP_STATUS}" "${TYPE}" "${MESSAGE}" "${DATE}"
			fi

		else

			./slackhook.sh "${BACKUP_STATUS}" "${TYPE}" "${MESSAGE}" "${DATE}"

		fi
	
	fi
}


backupProject() {
	for i in ${APP_LOCATION[@]}; do
	app_name=$(getAppName $i)	# get App Name (Last name of full path)


	b=$(./get_env.sh APP_BACKUP_LOCATION)/${app_name}  	# This contains backup path with app name
	BACKUP_LOCATION=$b/${FOLDER_DATE_FORMAT}			# This contains backup path with app name and folder 
	mkdir -p ${BACKUP_LOCATION}

	BACKUP_LOCATION_WITH_FILE="${BACKUP_LOCATION}/${FILE_DATE_FORMAT}.tar.gz";
	FILE_NAME="$(echo ${BACKUP_LOCATION_WITH_FILE} | rev | cut -d "/" -f1-3 | rev | sed -e 's/\//_/g')"


	exclude_flags=""
	APP_APP_EXCLUDE_DIR=($(./get_env.sh APP_APP_EXCLUDE_DIR))
	# Check if the APP_APP_EXCLUDE_DIR is not empty
	if [ ${#APP_APP_EXCLUDE_DIR[@]} -ne 0 ]; then
		for excludes in "${APP_APP_EXCLUDE_DIR[@]}"; do
			exclude_flags+=" --exclude=${excludes}"
		done
	fi

	# include directories
	[[ -n "$(./get_env.sh "$app_name"_INCLUDE_DIR)" ]] && include_directories="$(./get_env.sh "$app_name"_INCLUDE_DIR)" || include_directories="$(./get_env.sh APP_APP_INCLUDE_DIR)"


	# tar -czvf ${BACKUP_LOCATION_WITH_FILE} ${exclude_flags} -C $i .		# tar as relative path
	tar -czvf ${BACKUP_LOCATION_WITH_FILE} ${exclude_flags} -C $i $include_directories	# tar as relative path
	BACKUP_STATUS=$?

	MESSAGE=""
	if [[ ${BACKUP_STATUS} -eq 0 ]]; then
		MESSAGE="${FILE_NAME} successully backed up";
	else
		MESSAGE="Failed to backup ${FILE_NAME}";
	fi
	sendSlackNotification "${BACKUP_STATUS}" "app" "${MESSAGE}" ""

	if [[ ${BACKUP_STATUS} -eq 0 ]]; then
		uploadToS3 "${BACKUP_LOCATION_WITH_FILE}"
	fi

	purgeOldBackups "$b"
done
}

backupProject

# main "$@"

# includeDirectory
# echo $include_directories;
# exit 1;


