#!/bin/bash

set -eu
# set -o pipefail

################################################################################
# Help                                                                         #
################################################################################
help() {
  cat << EOF
Backup database
usage: $0 [OPTIONS]
    --help               Show this message
EOF
}


## Show help if input is -h or --help
main() {
    if [[ "$1" == "-h" || "$1" == '--help' ]] ; then
      help
      exit 0
    fi
}

# FOLDER_DATE_FORMAT
[[ -n "$(./get_env.sh DB_BACKUP_FOLDER_DATE_FORMAT)" ]] && FOLDER_DATE_FORMAT="$($(./get_env.sh DB_BACKUP_FOLDER_DATE_FORMAT))" || FOLDER_DATE_FORMAT="$(date +%F)"

# FILE_DATE_FORMAT
[[ -n "$(./get_env.sh DB_BACKUP_FILE_DATE_FORMAT)" ]] && FILE_DATE_FORMAT="$($(./get_env.sh DB_BACKUP_FILE_DATE_FORMAT))" || FILE_DATE_FORMAT="$(date +%F_%H-%M-%S)"


sendSlackNotification() {
	BACKUP_STATUS=$1		# Here backup status means backup or upload status
	TYPE=$2
	MESSAGE=$3
	DATE=$4

	# Send if enabled
	if [[ -n "$(./get_env.sh DB_SLACK_NOTIF)" && $(./get_env.sh DB_SLACK_NOTIF) == "true" ]]; then

		# Check whether to send a notification for success event
		if [[ "$BACKUP_STATUS" -eq 0 ]]; then

			if [[ -z "$(./get_env.sh DB_HIDE_SUCCESS_SLACK_NOTIF)" || "$(./get_env.sh DB_HIDE_SUCCESS_SLACK_NOTIF)" != "true" ]]; then
				./slackhook.sh "${BACKUP_STATUS}" "${TYPE}" "${MESSAGE}" "${DATE}"
			fi

		else

			./slackhook.sh "${BACKUP_STATUS}" "${TYPE}" "${MESSAGE}" "${DATE}"

		fi
		
	fi
}

dumpDb() {
	DBNAME=$1
	DB0_IGNORE_TABLE_NAMES=($(./get_env.sh DB_DB0_IGNORE_TABLE_NAMES))
	# read -a DB0_IGNORE_TABLE_NAMES <<< "$DB0_IGNORE_TABLE_NAMES"
	ignore_table_params=""
	
	# Check if the DB0_IGNORE_TABLE_NAMES is not empty
	if [ ${#DB0_IGNORE_TABLE_NAMES[@]} -ne 0 ]; then
		for table in "${DB0_IGNORE_TABLE_NAMES[@]}"; do
			ignore_table_params+=" --ignore-table=${DBNAME}.${table}"
		done
	fi

	# echo $ignore_table_params
	# exit 0
	

	b=$(./get_env.sh DB_BACKUP_LOCATION)/${DBNAME}  	# This contains backup path with database name
	BACKUP_LOCATION=$b/${FOLDER_DATE_FORMAT}			# This contains backup path with database name and folder 
	mkdir -p ${BACKUP_LOCATION}

	BACKUP_LOCATION_WITH_FILE="${BACKUP_LOCATION}/${FILE_DATE_FORMAT}.sql.gz";
	FILE_NAME="$(echo ${BACKUP_LOCATION_WITH_FILE} | rev | cut -d "/" -f1-3 | rev | sed -e 's/\//_/g')"

	# Start dumping 
	if [[ -n "$(./get_env.sh DB_UNIX_SOCKET)" && "$(./get_env.sh DB_UNIX_SOCKET)" == "true" ]]; then
		sudo mysqldump -v ${DBNAME} | gzip > ${BACKUP_LOCATION_WITH_FILE}
	else
		if [ -n \$ignore_table_params ]; then  # dump db excluding tables
		mysqldump --defaults-file='./.mysqldumpcred' -v $ignore_table_params ${DBNAME} | gzip > ${BACKUP_LOCATION_WITH_FILE}
		else
		mysqldump --defaults-file='./.mysqldumpcred' -v ${DBNAME} | gzip > ${BACKUP_LOCATION_WITH_FILE}
		fi
	fi

	BACKUP_STATUS=${PIPESTATUS[0]}

	MESSAGE=""
	if [[ ${BACKUP_STATUS} -eq 0 ]]; then
		MESSAGE="$FILE_NAME successully dumped";
	else
		MESSAGE="Failed to dump $FILE_NAME";
	fi
	sendSlackNotification "${BACKUP_STATUS}" "database" "$MESSAGE" ""
	# ./slackhook.sh ${BACKUP_STATUS} "database" "${MESSAGE}" ""	

	# Upload to AWS
	if [[ ${BACKUP_STATUS} -eq 0 ]]; then
		uploadToS3 "$BACKUP_LOCATION_WITH_FILE"
	fi

	purgeOldDbBackups $b

}

dumpAllDb() {

	DAYOFWEEK="$(date +%u)"		# Start: 1 [1 is Monday]
	DB_ALL_DB_DAY=$(./get_env.sh DB_ALL_DB_DAY)
	# We'll dump all db on every Saturday
	if [[ "${DAYOFWEEK}" -eq $DB_ALL_DB_DAY ]]; then

		b=$(./get_env.sh DB_BACKUP_LOCATION)/"all_db"  		# This contains backup path with database name (i,e, all_db)
		BACKUP_LOCATION=$b/${FOLDER_DATE_FORMAT}			# This contains backup path with database name and folder 
		mkdir -p ${BACKUP_LOCATION}

		BACKUP_LOCATION_WITH_FILE="${BACKUP_LOCATION}/${FILE_DATE_FORMAT}.sql.gz";
		FILE_NAME="$(echo ${BACKUP_LOCATION_WITH_FILE} | rev | cut -d "/" -f1-3 | rev | sed -e 's/\//_/g')"

		# Check if older db exists today (Max 1 is allowed per day)
		if [[ "$(find $BACKUP_LOCATION/* -type f | wc -l)" -ge 1 ]]; then
			echo "Max 1 \"all_db\" backups allowed in a day"
		else 

			# Start dumping 
			if [[ -n "$(./get_env.sh DB_UNIX_SOCKET)" && "$(./get_env.sh DB_UNIX_SOCKET)" == "true" ]]; then
				sudo mysqldump -v --all-databases | gzip > ${BACKUP_LOCATION_WITH_FILE}
			else
				mysqldump --defaults-file='./.mysqldumpcred' -v --all-databases | gzip > ${BACKUP_LOCATION_WITH_FILE}
			fi

			BACKUP_STATUS=${PIPESTATUS[0]}

			MESSAGE=""
			if [[ ${BACKUP_STATUS} -eq 0 ]]; then
				MESSAGE="$FILE_NAME successully dumped";
			else
				MESSAGE="Failed to dump $FILE_NAME";
			fi
			sendSlackNotification "${BACKUP_STATUS}" "database" "$MESSAGE" ""

			# Upload to AWS
			if [[ ${BACKUP_STATUS} -eq 0 ]]; then
				uploadToS3 "$BACKUP_LOCATION_WITH_FILE"
			fi

			####### Purge Old "all_db"
			purgeOldAllDbBackups "$b"

		fi
		
	fi
}

purgeOldDbBackups() {
	### Delete all backups older than "DB_MAX_BACKUP_DAYS" 
	# older_backups="find ${APP_BACKUP_PATH} -maxdepth 1 -type d -mtime +${DB_MAX_BACKUP_DAYS}"

	DB_BACKUP_PATH="$1/*"
	[[ -n "$(./get_env.sh DB_MAX_BACKUP_DAYS)" ]] && DB_MAX_BACKUP_DAYS="$(./get_env.sh DB_MAX_BACKUP_DAYS)" || DB_MAX_BACKUP_DAYS="15"

	find_exp="find ${DB_BACKUP_PATH} -maxdepth 1 -type d"
	total_backups="$($find_exp | wc -l)" 			# total backup count
	older_backups="$find_exp -mtime +${DB_MAX_BACKUP_DAYS}"		# older than X days
	older_backups_count="$($(echo "${older_backups}") | wc -l)"
	newer_backups_count="$($(echo "$find_exp -mtime -${DB_MAX_BACKUP_DAYS}") | wc -l)"

	# remaining(newer) backups must be equal to max_backups
	if [[ "${newer_backups_count}" -ge ${DB_MAX_BACKUP_DAYS} ]]; then
		echo "Deleting $older_backups_count older backups";
		$(echo "${older_backups}") | xargs rm -rf
	else
		echo "Older backups can not be deleted to preserve existing backups. Please try again."
	fi
	
}

### This will purge older "all_db" databases
purgeOldAllDbBackups() {
	
	DB_BACKUP_PATH="$1/*"
	DB_MAX_BACKUP_DAYS="60"		# 2 months

	find_exp="find ${DB_BACKUP_PATH} -maxdepth 1 -type d"
	total_backups="$($find_exp | wc -l)" 			# total backup count
	older_backups="$find_exp -mtime +${DB_MAX_BACKUP_DAYS}"		# older than X days
	older_backups_count="$($(echo "${older_backups}") | wc -l)"
	newer_backups_count="$($(echo "$find_exp -mtime -${DB_MAX_BACKUP_DAYS}") | wc -l)"

	# remaining(newer) backups must be equal to max_backups
	if [[ "${newer_backups_count}" -ge ${DB_MAX_BACKUP_DAYS} ]]; then
		echo "Deleting $older_backups_count older backups";
		$(echo "${older_backups}") | xargs rm -rf
	else
		echo "Older backups are not found and can not be deleted."
	fi
	
}

getLastName() {
	IFS='/' read -ra ADDR <<< "${1}"	
	echo ${ADDR[-1]}
}

uploadToS3() {
	# Backup if enabled
	if [[ -n "$(./get_env.sh DB_S3_BACKUP)" && $(./get_env.sh DB_S3_BACKUP) == "true" ]]; then

		FILE_NAME="$(echo $1 | rev | cut -d "/" -f1-3 | rev | sed -e 's/\//_/g')"
		FOLDER_PATH="$(echo $1 | rev | cut -d "/" -f2- | cut -d "/" -f1-2 | rev)"
		FILE_PATH=$1

		if [[ -n "$(./get_env.sh USE_CUSTOM_S3CMD)" && $(./get_env.sh USE_CUSTOM_S3CMD) == "true" ]]; then
			S3_BIN_FILE="../bin/s3cmd-2.2.0/s3cmd"
		else 
			S3_BIN_FILE="s3cmd"
		fi

		# s3cmd put "${FILE_PATH}" s3://"$(./get_env.sh DB_S3_LOCATION)"/${FOLDER_PATH}/
		$S3_BIN_FILE -c "$(./get_env.sh DB_S3_CONFIG_FILE)" put "${FILE_PATH}" "$(./get_env.sh DB_S3_LOCATION)"${FOLDER_PATH}/
		UPLOAD_STATUS=$?
		# if secondary config exists; proceed the backup
		if [[ -n "$(./get_env.sh DB_S3_SECONDARY_CONFIG_FILE)" ]]; then
			$S3_BIN_FILE -c "$(./get_env.sh DB_S3_SECONDARY_CONFIG_FILE)" put "${FILE_PATH}" "$(./get_env.sh DB_S3_SECONDARY_LOCATION)"${FOLDER_PATH}/
		fi

		MESSAGE=""
		if [[ ${UPLOAD_STATUS} -eq 0 ]]; then
			MESSAGE="${FILE_NAME} successully uploaded to S3";
		else
			MESSAGE="Failed to upload ${FILE_NAME} to S3";
		fi
		sendSlackNotification "${UPLOAD_STATUS}" "aws" "${MESSAGE}" ""

	fi
}


# main "$@"

DB_NAMES=($(./get_env.sh DB_DB_NAMES))
for db in "${DB_NAMES[@]}"; do
	dumpDb "$db"
done


# Dump & Purge All Db
# Backup if enabled
if [[ -n "$(./get_env.sh DB_ALL_DB_BACKUP)" && $(./get_env.sh DB_ALL_DB_BACKUP) == "true" ]]; then
	dumpAllDb
fi
