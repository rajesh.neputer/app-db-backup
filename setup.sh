#!/bin/bash

cd please-do-not-delete/scripts
find ./* -name "*.sh" | xargs chmod +x
cp .env.example .env
cp .mysqldumpcred.example .mysqldumpcred
